//
//  UIImageView.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 06/06/24.
//

import UIKit
import Kingfisher
extension UIImageView{
    ///This function shows an image by an URL using Kingfisher
    public func setImage(fromUrl: String,defaultImage:UIImage=UIImage()) {
        let fullArr = fromUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let url = URL(string: fullArr)
        if let url = url {
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url)
            self.contentMode = .scaleAspectFill
        }else{
            self.image=defaultImage
        }
    }
}
