//
//  UIView.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import UIKit
extension UIView{
    ///Function used to define corner radius with half its height
    public func rounded(){
        layer.cornerRadius = frame.height/2
        layer.rasterizationScale = UIScreen.main.scale
    }
}
