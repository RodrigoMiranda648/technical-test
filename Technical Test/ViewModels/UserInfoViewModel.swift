//
//  UserInfoViewModel.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import Foundation
import Combine

class UserInfoViewModel: ObservableObject{
    ///This propertie receives Breed List from the API is monitored by the VC
    @Published var data: [Breed]? = nil
    ///This propertie stores an error message from the API is monitored by the VC
    @Published var errorMessage: String? = nil
    ///This cancellable stores the NetworkManager request
    private var cancellable: AnyCancellable?
    let url = "https://api.thedogapi.com/v1/breeds"

    ///Method used to get the breed data
    func loadData() {
        cancellable = NetworkManager.fetchData(from: url)
            .sink(receiveCompletion: { [weak self] completion in //make self weak to avoid retain cycles
                switch completion {
                case .finished:
                    print("Data was received successfully.")
                case .failure(let error):
                    print("Error: \(error)")
                    self?.errorMessage = error.localizedDescription //The error message is set this will update the UI in the VC
                }
                //Since we don't neet the subscription anymore, the cancellable is released
                self?.cancellable?.cancel()
                self?.cancellable = nil
            }, receiveValue: { [weak self] breeds in // define self as weak to avoid retain cycles
                self?.data = breeds //The data is set, this will update the UI in the VC
            })
    }
}
