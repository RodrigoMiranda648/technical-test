//
//  UserInfoView.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import UIKit

/// This clas is used to show what is above the table
class UserInfoView: UIView{
//MARK: UIElements
    ///This stack contains UIElements of the class
    private let stackVContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///This stack contains my personal information
    private let stackHPersonalInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 20
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///This stack contains the degree information
    private let stackVDegreeInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///ImageView of the person
    private let imgProfile: UIImageView = {
        let img = UIImage(named: "img.Profile")
        let image = UIImageView(image: img)
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    ///Label of person name
    private let lblName: UILabel = {
        let lbl = UILabel()
        lbl.text = "Rodrigo Miranda Castillo"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Label that contains my degree
    private let lblDegree: UILabel = {
        let lbl = UILabel()
        lbl.text = "Ingeniería Mecánica"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Label that contains the finish date
    private let lblDate: UILabel = {
        let lbl = UILabel()
        lbl.text = "06/Junio/2024"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///View used to adjust degree labels according to design
    private let viewSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    ///Separator view from personal information and the table
    private let viewSectionSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    ///This stack contains table's header
    private let stackHTableHeader: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 4
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///Header id label
    private let lblID: UILabel = {
        let lbl = UILabel()
        lbl.text = "Id"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor(named: "color.header")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Header name label
    private let lblAnimalName: UILabel = {
        let lbl = UILabel()
        lbl.text = "Nombre"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor(named: "color.header")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Header breed label
    private let lblBreed: UILabel = {
        let lbl = UILabel()
        lbl.text = "Raza"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor(named: "color.header")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Header description label
    private let lblDescription: UILabel = {
        let lbl = UILabel()
        lbl.text = "Descripción"
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor(named: "color.header")
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
//MARK: Properties
    ///Font size for personal information
    let infoFontSize: CGFloat = 30
    ///Font size for header elements
    let headerFontSize: CGFloat = 28
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        setupUI()
        setupConstraints()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//MARK: methods
    private func setupUI(){
        [lblName, lblDegree, lblDate].forEach { lbl in
            lbl.font = UIFont.systemFont(ofSize: infoFontSize, weight: .bold)
        }
        [lblID, lblAnimalName, lblBreed, lblDescription].forEach { lbl in
            lbl.font = UIFont.systemFont(ofSize: headerFontSize, weight: .black)
        }
        imgProfile.layer.cornerRadius = 50
    }
    private func setupConstraints(){
        self.addSubview(stackVContainer)
        stackVContainer.addArrangedSubview(stackHPersonalInfo)

        stackHPersonalInfo.addArrangedSubview(imgProfile)
        stackHPersonalInfo.addArrangedSubview(lblName)
        stackHPersonalInfo.addArrangedSubview(stackVDegreeInfo)

        stackVDegreeInfo.addArrangedSubview(lblDegree)
        stackVDegreeInfo.addArrangedSubview(lblDate)
        stackVDegreeInfo.addArrangedSubview(viewSeparator)

        stackVContainer.addArrangedSubview(viewSectionSeparator)
        
        stackVContainer.addArrangedSubview(stackHTableHeader)
        stackHTableHeader.addArrangedSubview(lblID)
        stackHTableHeader.addArrangedSubview(lblAnimalName)
        stackHTableHeader.addArrangedSubview(lblBreed)
        stackHTableHeader.addArrangedSubview(lblDescription)
                
        NSLayoutConstraint.activate([
            stackVContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackVContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackVContainer.topAnchor.constraint(equalTo: self.topAnchor),
            stackVContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            //Define fix constraints into personal info
            stackHPersonalInfo.heightAnchor.constraint(equalToConstant: 100),
            imgProfile.widthAnchor.constraint(equalToConstant: 100),
            stackVDegreeInfo.widthAnchor.constraint(equalTo: stackVContainer.widthAnchor, multiplier: 0.22),

            //Denife separator vertical size
            viewSectionSeparator.heightAnchor.constraint(equalToConstant: 10),

            //Define header constraints according to its container
            stackHTableHeader.heightAnchor.constraint(equalToConstant: 60),
            lblID.widthAnchor.constraint(equalTo: stackVContainer.widthAnchor, multiplier: 0.07),
            lblAnimalName.widthAnchor.constraint(equalTo: stackVContainer.widthAnchor, multiplier: 0.27),
            lblBreed.widthAnchor.constraint(equalTo: stackVContainer.widthAnchor, multiplier: 0.18)
        ])
    }
}
