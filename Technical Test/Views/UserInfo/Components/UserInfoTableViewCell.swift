//
//  UserInfoTableViewCell.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell {
    static let identifier = "UserInfoTableViewCell"
    ///This stack contains cell's info
    let stackHInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 4
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///The label for the id
    let lblID: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Stack used for animalName and its image
    private let stackHName: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 20
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///view used to add left padding to image and name
    private let viewNameIdentation: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    ///The animal image view
    let imgAnimal: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    ///The label for the id
    let lblName: UILabel = {
        let lbl = UILabel()
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///The label for the id
    let lblBreed: UILabel = {
        let lbl = UILabel()
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///Stack used for add padding to the lable
    private let stackHDescription: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///View used to add left padding to description label
    private let viewDescriptionLeftPadding: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    ///The label for the id
    let lblDescription: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .justified
        lbl.numberOfLines = 2
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.7
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    ///View used to add right padding to description label
    private let viewDescriptionRightPadding: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    ///Size for header fonts
    let fontSize: CGFloat = 25
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupConstraints()
        setupUI()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        imgAnimal.rounded()
        
    }
    private func setupUI(){
        [lblID,lblName, lblBreed, lblDescription].forEach { lbl in
            lbl.font = UIFont.systemFont(ofSize: fontSize, weight: .bold)
        }
    }
    ///This function is used to set data received from cell for row at method in the VC
    func setup(breed: Breed){
        lblID.text = "\(breed.id)"
        lblName.text = breed.name
        lblBreed.text = breed.breedGroup
        lblDescription.text = breed.temperament
        let url = breed.getImageURL()
        imgAnimal.setImage(fromUrl: url)
    }
    private func setupConstraints(){
        self.addSubview(stackHInfo)
        stackHInfo.addArrangedSubview(lblID)
        stackHInfo.addArrangedSubview(stackHName)
        stackHInfo.addArrangedSubview(lblBreed)
        stackHInfo.addArrangedSubview(stackHDescription)

        stackHName.addArrangedSubview(viewNameIdentation)
        stackHName.addArrangedSubview(imgAnimal)
        stackHName.addArrangedSubview(lblName)

        stackHDescription.addArrangedSubview(viewDescriptionLeftPadding)
        stackHDescription.addArrangedSubview(lblDescription)
        stackHDescription.addArrangedSubview(viewDescriptionRightPadding)
        
        NSLayoutConstraint.activate([
            //Set constraints mantaining a separation to show a blank space between cells
            stackHInfo.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackHInfo.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackHInfo.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            stackHInfo.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),

            //set identation size, and image size from the breed name
            viewNameIdentation.widthAnchor.constraint(equalToConstant: 30),
            imgAnimal.widthAnchor.constraint(equalTo: stackHInfo.heightAnchor, multiplier: 0.8),
            imgAnimal.heightAnchor.constraint(equalTo: imgAnimal.widthAnchor),

            //These 3 wide are equal than those defined in the header
            lblID.widthAnchor.constraint(equalTo: stackHInfo.widthAnchor, multiplier: 0.07),
            stackHName.widthAnchor.constraint(equalTo: stackHInfo.widthAnchor, multiplier: 0.27),
            lblBreed.widthAnchor.constraint(equalTo: stackHInfo.widthAnchor, multiplier: 0.18),

            //Set padding for the description
            viewDescriptionLeftPadding.widthAnchor.constraint(equalToConstant: 15),
            viewDescriptionRightPadding.widthAnchor.constraint(equalToConstant: 15)
        ])
    }
}
