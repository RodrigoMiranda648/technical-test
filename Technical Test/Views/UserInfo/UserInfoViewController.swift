//
//  UserInfoViewController.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import UIKit
import Combine
import Toast_Swift

class UserInfoViewController: UIViewController {
    ///The stack container for the entire page
    private let stackVContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    ///The view that contains the elements above the table
    private let contentView = UserInfoView()
    ///This table contains breed information
    private let tableView: UITableView = {
        let table = UITableView()
        table.rowHeight = 80
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.isPagingEnabled = true
        table.register(UserInfoTableViewCell.self, forCellReuseIdentifier: UserInfoTableViewCell.identifier)
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    ///Module's ViewModel Instance
    private let viewModel = UserInfoViewModel()
    ///Cancellables that will receive data from the viewModel
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        super.viewDidLoad()
        setupUI()
        layoutViews()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBindings()
        viewModel.loadData()
    }
    private func setupUI(){
        tableView.dataSource = self
    }
    private func layoutViews(){
        view.addSubview(stackVContainer)
        stackVContainer.addArrangedSubview(contentView)
        stackVContainer.addArrangedSubview(tableView)
        
        
        NSLayoutConstraint.activate([
            stackVContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30),
            stackVContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30),
            stackVContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            stackVContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
        ])
    }
    private func setupBindings() {
        //Method to receive changes from viewModel's properties
        //both data access will receive responses in the main thread to make UI changes.
        //The table will be reloaded or an error message will be shown depending on what data exists
        //Subscriptions are stored in ViewController's cancellables
        viewModel.$data
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                if let _ = data {
                    self?.tableView.reloadData()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$errorMessage
            .receive(on: DispatchQueue.main)
            .sink { [weak self] errorMessage in
                if let errorMessage = errorMessage {
                    print(errorMessage)
                    self?.view.makeToast(errorMessage, duration: 3, position: .center)
                }
            }
            .store(in: &cancellables)
        }
}
extension UserInfoViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Returns 20 if number or data filled is less, otherwise return the number of data
        return max(viewModel.data?.count ?? 0, 20)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserInfoTableViewCell.identifier, for: indexPath) as! UserInfoTableViewCell
        //Set the color depending on whether the number is even or odd
        cell.stackHInfo.backgroundColor = indexPath.row % 2 == 0 ? UIColor(named: "color.darkCell") : UIColor(named: "color.lightCell")
        guard let breed = viewModel.data?[indexPath.row] else {
            return cell
        }
        cell.setup(breed: breed)
        return cell
    }
}
