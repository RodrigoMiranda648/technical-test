//
//  UserInfoModel.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import Foundation

///Model of breeds that will be received by an API Rest
struct Breed: Decodable {
    let id: Int
    let name: String
    let breedGroup: String?
    let temperament: String?
    let referenceImageID: String

    ///Coding keys used to adjust keys strings in the request
    enum CodingKeys: String, CodingKey {
        case id, name, temperament
        case referenceImageID = "reference_image_id"
        case breedGroup = "breed_group"
    }
    ///Method to obtain the image url
    func getImageURL() -> String{
        return "https://cdn2.thedogapi.com/images/\(referenceImageID).jpg"
    }
}
