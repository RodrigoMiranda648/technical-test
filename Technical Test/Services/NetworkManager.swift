//
//  NetworkManager.swift
//  Technical Test
//
//  Created by Rodrigo Miranda on 05/06/24.
//

import Foundation
import Combine

class NetworkManager {
    ///function used to receive data from the API, returns an AnyPublisher or an error depending on the response
    static func fetchData(from urlString: String) -> AnyPublisher<[Breed], Error> {
        guard let url = URL(string: urlString) else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        //Create a publisher through URLSession
        return URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data) //Map data received
            .decode(type: [Breed].self, decoder: JSONDecoder()) //decode the data received
            .mapError { error in    //Map error data
                return error as Error   //Cast error as Error generic tipe
            }
            .eraseToAnyPublisher()  //Make the publisher an AnyPublisher
    }
}
